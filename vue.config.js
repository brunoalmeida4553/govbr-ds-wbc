module.exports = {
  lintOnSave: true,
  filenameHashing: false,
  css: { extract: false },
  chainWebpack: (config) => {
    config.optimization.delete('splitChunks')
  },
}
