# Web Components GOVBR-DS

Biblioteca baseada no [GOVBR-DS](https://gov.br/ds 'GOVBR-DS') e construída usando Web Components para ser independente de frameworks.

## Tecnologias

Esse projeto é desenvolvido usando:

1. [VueJS 3](https://v3.vuejs.org/ 'VueJS 3')
1. [Storybook](https://storybook.js.org/ 'Storybook')
1. [Jest](https://jestjs.io/ 'Jest')

Para saber mais detalhes sobre Web Components sugerimos que consulte o [MDN](https://developer.mozilla.org/pt-BR/docs/Web/Web_Components 'Web Components | MDN').

## Dependências

Para conseguir usar nossa biblioteca sem problemas temos as seguintes dependências:

1. [GOVBR-DS](https://gov.br/ds/ 'GOVBR-DS')

1. [Font Awesome](https://fontawesome.com/ 'Font Awesome')

1. [Fonte Rawline](https://www.cdnfonts.com/rawline.font/ 'Fonte Rawline')

> O fontawesome e a fonte rawline podem ser importadas de um CDN. Consulte a documentação no site do [GOVBR-DS](https://www.gov.br/ds/ 'GOVBR-DS') para mais detalhes.

## Instalação

```bash

npm install --save @govbr-ds/webcomponents
```

## Uso

Depois de instalada, importe a biblioteca de dentro da pasta *node_modules*:

```javascript

@import "node_modules/@govbr-ds/webcomponents/dist/webcomponents.umd.min.js"
```

### Exemplos de uso

Disponibilizamos alguns exemplos de como usar esse projeto com algumas tecnologias. Consulte o [nosso grupo aqui no gitlab](https://gitlab.com/govbr-ds/dev/wbc 'GOVBR-DS/DEV/WBC') e procure pelos projetos de 'Quickstart' para mais detalhes.

## Rodando localmente

Clone o projeto:

```bash

git clone git@gitlab.com:govbr-ds/dev/wbc/govbr-ds-wbc.git
```

Entre no diretório do projeto:

```bash

cd govbr-ds-wbc
```

Instale as dependências:

```bash

npm install
```

Para executar a nossa biblioteca a partir do código-fonte você tem 2 opções:

###  Executar como VueJS Components

Nessa visualização você executa o projeto em uma página auxiliar (fora do storybook) e sem compilar o resultado para Web Components.

```node

npm run serve
```

###  Executar como Web Components

Nessa visualização o projeto é executado a partir do storybook e convertido para Web Components.

Aqui você consegue testar as configurações, obter snippets de código e visualizar os componentes rodando como nos ambientes de produção.

```node

npm run storybook
```

Essa compilação pode demorar para refletir as alterações na página. Assim, recomendamos que utilizem primeiro a execução como VueJS durante o desenvolvimento e quando considerarem as alterações maduras o suficiente façam os testes como Web Components.

## Documentação

Nossa documentação está disponível em <https://gov.br/ds/webcomponents/>.

## Contribuindo

Antes de abrir um Merge Request tenha em mente algumas informações:

-   Esse é um projeto opensource e contribuições são bem-vindas.
-   Para facilitar a aprovação da sua contribuição, escolha um título curto, simples e explicativo para o MR, e siga os padrões da nossa [wiki](https://govbr-ds.gitlab.io/govbr-ds-wiki/ 'Wiki').
-   Quer contribuir com o projeto? Confira o nosso guia [como contribuir](./CONTRIBUTING.md 'Como contribuir?').

### Reportar bugs/necessidades

Você pode usar as [issues](https://gitlab.com/govbr-ds/dev/wbc/govbr-ds-wbc/-/issues/new) para nos informar os problemas que tem enfrentado ao usar nossa biblioteca ou mesmo o que gostaria que fizesse parte do projeto. Por favor use o modelo que mais se encaixa na sua necessidade e preencha com o máximo de detalhes possível.

Nos comprometemos a responder a todas as issues.

## Precisa de ajuda?

> Por favor **não** crie issues para fazer perguntas...

Use nossos canais abaixo para obter tirar suas dúvidas:

-   Site do GOVBR-DS <http://gov.br/ds>

-   Web Components <https://gov.br/ds/webcomponents/>

-   Pelo nosso email <govbr-ds@serpro.gov.br>

-   Usando nosso canal no discord <https://discord.gg/U5GwPfqhUP>

## Commits

Nesse projeto usamos um padrão para branches e commits. Por favor observe a documentação na nossa [wiki](https://govbr-ds.gitlab.io/govbr-ds-wiki/ 'Wiki') para aprender sobre os nossos padrões.

## Apêndice

### Polyfill

Caso os requisitos to seu projeto incluam navegadores que não suportam completamente os Web Components, adicione os [polyfills](https://www.webcomponents.org/polyfills) ao seu projeto.

Para aprender com instalar, consulte a [documentação oficial](https://github.com/webcomponents/polyfills/tree/master/packages/webcomponentsjs 'Polyfills')## Créditos

Os Web Components do [GOVBR-DS](https://gov.br/ds/ 'GOVBR-DS') são criados pelo [SERPRO](https://www.serpro.gov.br/ 'SERPRO | Serviço Federal de Processamento de Dados') e [Dataprev](https://www.dataprev.gov.br/ 'Dataprev | Empresa de Tecnologia e Informações da Previdência') juntamente com a participação da comunidade.

## Licença

Nesse projeto usamos a licença MIT.
