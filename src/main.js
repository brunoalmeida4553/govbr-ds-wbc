import '@govbr-ds/core/dist/core-base.css'
import { defineCustomElement } from 'vue'
import Avatar from './components/Avatar/Avatar.ce.vue'
import BrBreadcrumb from './components/Breadcrumb/Breadcrumb.ce.vue'
import Button from './components/Button/Button.ce.vue'
import BrCard from './components/Card/Card.ce.vue'
import BrCardContent from './components/Card/CardContent.ce.vue'
import BrCardFooter from './components/Card/CardFooter.ce.vue'
import BrCardHeader from './components/Card/CardHeader.ce.vue'
import BrCheckbox from './components/Checkbox/Checkbox.ce.vue'
import BrCheckgroup from './components/Checkbox/Checkgroup.ce.vue'
import Divider from './components/Divider/Divider.ce.vue'
import BrFooter from './components/Footer/Footer.ce.vue'
import BrHeader from './components/Header/Header.ce.vue'
import BrHeaderAction from './components/Header/HeaderAction.ce.vue'
import BrHeaderSearch from './components/Header/HeaderSearch.ce.vue'
import IconBase from './components/IconBase/IconBase.ce.vue'
import BrInput from './components/Input/Input.ce.vue'
import BrItem from './components/Item/Item.ce.vue'
import BrList from './components/List/List.ce.vue'
import BrLoading from './components/Loading/Loading.ce.vue'
import MagicButton from './components/MagicButton/MagicButton.ce.vue'
import BrMenu from './components/Menu/Menu.ce.vue'
import Message from './components/Message/Message.ce.vue'
import Notification from './components/Notification/Notification.ce.vue'
import BrSignIn from './components/SignIn/SignIn.ce.vue'
import BrSwitch from './components/Switch/Switch.ce.vue'
import Tab from './components/Tab/Tab.ce.vue'
import TabItem from './components/Tab/TabItem.ce.vue'

const componentsConfig = [
  { tagName: 'br-avatar', component: Avatar },
  { tagName: 'br-button', component: Button },
  { tagName: 'br-magic-button', component: MagicButton },
  { tagName: 'br-card', component: BrCard },
  { tagName: 'br-checkbox', component: BrCheckbox },
  { tagName: 'br-checkgroup', component: BrCheckgroup },
  { tagName: 'br-card-header', component: BrCardHeader },
  { tagName: 'br-card-content', component: BrCardContent },
  { tagName: 'br-card-footer', component: BrCardFooter },
  { tagName: 'br-divider', component: Divider },
  { tagName: 'br-input', component: BrInput },
  { tagName: 'br-tab', component: Tab },
  { tagName: 'br-loading', component: BrLoading },
  { tagName: 'br-tab-item', component: TabItem },
  { tagName: 'br-list', component: BrList },
  { tagName: 'br-item', component: BrItem },
  { tagName: 'br-switch', component: BrSwitch },
  { tagName: 'br-breadcrumb', component: BrBreadcrumb },
  { tagName: 'br-message', component: Message },
  { tagName: 'br-footer', component: BrFooter },
  { tagName: 'br-header', component: BrHeader },
  { tagName: 'br-header-action', component: BrHeaderAction },
  { tagName: 'br-header-search', component: BrHeaderSearch },
  { tagName: 'br-menu', component: BrMenu },
  { tagName: 'br-notification', component: Notification },
  { tagName: 'br-sign-in', component: BrSignIn },
  { tagName: 'br-icon-base', component: IconBase },
]

componentsConfig.forEach((config) => {
  customElements.define(config.tagName, defineCustomElement(config.component))
})
