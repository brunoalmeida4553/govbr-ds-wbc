import { shallowMount } from '@vue/test-utils'
import Button from './../Button/Button.ce.vue'
import Divider from './../Divider/Divider.ce.vue'
import BrNotification from './Notification.ce.vue'

describe('br-notification', () => {
  const showNotification = true
  const titleExample = 'title example'
  const subtitleExample = 'subtitle example'
  const notificationsItens = `[
  {
    tab: { title: 'Links' },
    itens: [
      {icon: 'heartbeat', text: 'Link de acesso'},
      {icon: 'heartbeat', text: 'Link de acesso'},
      {icon: 'heartbeat', text: 'Link de acesso'}
    ],
    tab: { title: 'Links' },
    itens: [
      {icon: 'heartbeat', text: 'Link de acesso'},
      {icon: 'heartbeat', text: 'Link de acesso'},
      {icon: 'heartbeat', text: 'Link de acesso'}
    ]
  }
]`

  test('check if renders class br-notification', () => {
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        showNotification: showNotification,
        title: titleExample,
        subtitle: subtitleExample,
        notifications: notificationsItens,
      },
      global: {
        stubs: {
          'br-button': Button,
          'br-divider': Divider,
        },
      },
    })
    expect(wrapper.find('.br-notification').exists()).toBe(true)
  })

  test('check if renders class notification-header ', () => {
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        showNotification: showNotification,
        title: titleExample,
        subtitle: subtitleExample,
        notifications: notificationsItens,
      },
      global: {
        stubs: {
          'br-button': Button,
          'br-divider': Divider,
        },
      },
    })
    expect(wrapper.find('.notification-header').exists()).toBe(true)
  })

  test('check if renders class notification-body ', () => {
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        showNotification: showNotification,
        title: titleExample,
        subtitle: subtitleExample,
        notifications: notificationsItens,
      },
      global: {
        stubs: {
          'br-button': Button,
          'br-divider': Divider,
        },
      },
    })
    expect(wrapper.find('.notification-body').exists()).toBe(true)
  })

  test('check if prop title its visible.', () => {
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        showNotification: showNotification,
        title: titleExample,
        subtitle: subtitleExample,
        notifications: notificationsItens,
      },
      global: {
        stubs: {
          'br-button': Button,
          'br-divider': Divider,
        },
      },
    })
    expect(wrapper.text()).toMatch(titleExample)
  })

  test('check if prop subtitle its visible.', () => {
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        showNotification: showNotification,
        title: titleExample,
        subtitle: subtitleExample,
        notifications: notificationsItens,
      },
      global: {
        stubs: {
          'br-button': Button,
          'br-divider': Divider,
        },
      },
    })
    expect(wrapper.text()).toMatch(subtitleExample)
  })

  test('check if notifications are visible', () => {
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        showNotification: showNotification,
        title: titleExample,
        subtitle: subtitleExample,
        notifications: notificationsItens,
      },
      global: {
        stubs: {
          'br-button': Button,
          'br-divider': Divider,
        },
      },
    })

    expect(wrapper.findAll('.br-item')).toHaveLength(3)
  })

  test('check if close button its push', () => {
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        showNotification: showNotification,
        title: titleExample,
        subtitle: subtitleExample,
        notifications: notificationsItens,
      },
      global: {
        stubs: {
          'br-button': Button,
          'br-divider': Divider,
        },
      },
    })
    const closeNotification = wrapper.find('.col-2')
    closeNotification.trigger('click')
    wrapper.vm.close()
  })

  test('check if tab button its push', () => {
    const wrapperTab = shallowMount(BrNotification, {
      propsData: {
        showNotification: showNotification,
        title: titleExample,
        subtitle: subtitleExample,
        notifications: notificationsItens,
      },
      global: {
        stubs: {
          'br-button': Button,
          'br-divider': Divider,
        },
      },
    })
    const selectedTab = wrapperTab.find('.tab-item')
    selectedTab.trigger('click')
    wrapperTab.vm.selectTab
  })

  test('check notification without header', () => {
    const wrapper = shallowMount(BrNotification, {
      propsData: {
        showNotification: showNotification,
        disableCloseButton: true,
        notifications: notificationsItens,
      },
      global: {
        stubs: {
          'br-button': Button,
          'br-divider': Divider,
        },
      },
    })
    expect(wrapper.find('.notification-header').exists()).toBe(false)
  })
})
