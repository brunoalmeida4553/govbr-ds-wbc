import { kebabiseArgs } from '../../util/Utils.js'
import BrAvatar from './Avatar.ce.vue'

export default {
  title: 'Componentes/Avatar',
  component: BrAvatar,
  argTypes: {
    name: {
      control: 'text',
    },
    iconic: {
      control: 'boolean',
    },
    density: {
      control: { type: 'select', options: ['small', 'medium', 'large'] },
    },
  },
}

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-avatar v-bind="args"></br-avatar>`,
})

export const Iconico = Template.bind({})
Iconico.args = {
  name: 'Jhon Doe',
  iconic: true,
}

export const Fotografico = Template.bind({})
Fotografico.args = {
  name: 'Jhon Doe',
  iconic: false,
  image: 'https://picsum.photos/id/823/400',
}

export const Letra = Template.bind({})
Letra.args = {
  name: 'Jhon Doe',
  iconic: false,
}
