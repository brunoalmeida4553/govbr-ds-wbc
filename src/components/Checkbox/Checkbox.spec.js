import { shallowMount } from '@vue/test-utils'
import Checkbox from './Checkbox.ce.vue'

describe('Checkbox', () => {
  test('it renders br-checkbox', () => {
    const wrapper = shallowMount(Checkbox)
    expect(wrapper.classes('br-checkbox')).toBe(true)
  })

  test('set label attribute', () => {
    const rotulo = 'Label do checkbox'
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        label: rotulo,
      },
    })
    expect(wrapper.text()).toMatch(rotulo)
  })

  test('set inline attribute', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        inline: true,
      },
    })
    expect(wrapper.find('.br-checkbox.d-inline').exists()).toBe(true)
  })

  test('set disabled attribute', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        disabled: true,
      },
    })
    expect(wrapper.props('disabled')).toBe(true)
  })

  test('set format attribute = valid', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        format: 'valid',
      },
    })
    expect(wrapper.find('.br-checkbox.valid').exists()).toBe(true)
  })

  test('set format attribute = invalid', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        format: 'invalid',
      },
    })
    expect(wrapper.find('.br-checkbox.invalid').exists()).toBe(true)
  })

  test('set indeterminate checkbox', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        indeterminate: true,
      },
    })
    expect(wrapper.props('indeterminate')).toBe(true)
  })

  test('set checked checkbox', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        checked: true,
      },
    })
    expect(wrapper.props('checked')).toBe(true)
  })

  test('has the expected html structure', () => {
    const wrapper = shallowMount(Checkbox)
    expect(wrapper.element).toMatchSnapshot()
  })

  test('has the expected html structure', () => {
    const wrapper = shallowMount(Checkbox)
    expect(wrapper.element).toMatchSnapshot()
  })

  test('set checkbox checked', () => {
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        indeterminate: true,
      },
    })
    const checkbox = wrapper.find('.br-checkbox')
    checkbox.setChecked()
    expect(checkbox.element.checked).toBeTruthy()
  })
  test('call toogleChecked method', () => {
    const toogleChecked = jest.spyOn(Checkbox.methods, 'toogleChecked')
    const wrapper = shallowMount(Checkbox, {
      propsData: {
        checked: false,
      },
    })
    const checkbox = wrapper.find('input[type="checkbox"]')
    checkbox.trigger('input')
    checkbox.trigger('change')
    expect(toogleChecked).toBeCalled()
  })
})
