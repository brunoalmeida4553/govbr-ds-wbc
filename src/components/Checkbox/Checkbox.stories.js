import { customEventStorybookArgType, kebabiseArgs } from '../../util/Utils.js'
import Checkbox from '../Checkbox/Checkbox.ce.vue'

export default {
  title: 'Componentes/Checkbox',
  component: Checkbox,
  parameters: {
    controls: { exclude: ['value', 'v-model'] },
  },
  argTypes: {
    /** **[OPCIONAL]** Texto descritivo, localizado sempre à direita da caixa de opção.*/
    label: {
      defaultValue: '',
    },
    /** **[OPCIONAL]** Desabilita o checkbox. */
    disabled: {
      defaultValue: false,
    },
    format: {
      control: {
        type: 'select',
        options: ['default', 'valid', 'invalid'],
      },
    },
    /** **[OPCIONAL]** Formata o componente para versão horizontal. */
    inline: {
      defaultValue: false,
    },
    /** **[OPCIONAL]** Acessibilidade: define uma cadeia de caracteres para descrever o elemento. */
    ariaLabel: {
      defaultValue: '',
    },
    /** **[OPCIONAL]** Define o name que será atribuido ao checkbox. */
    name: {
      defaultValue: '',
    },
    /** Estado checked. */
    checked: {
      type: 'boolean',
      defaultValue: false,
    },
    change: {
      ...customEventStorybookArgType,
    },
  },
}

const defaultExcluded = [
  'default',
  'indeterminate',
  'checkgroupHeader',
  'model',
  'vModel',
]

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-checkbox v-bind="args"></br-checkbox>`,
})

export const Base = Template.bind({})
Base.args = {
  label: 'Label do checkbox',
  name: 'base',
  model: 'check1',
}
Base.parameters = {
  controls: {
    exclude: defaultExcluded,
  },
}

export const WithoutLabel = Template.bind({})
WithoutLabel.args = {
  vModel: 'check2',
}
WithoutLabel.parameters = {
  controls: {
    exclude: defaultExcluded,
  },
}

const TemplateChecked = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-checkbox v-bind="args" checked></br-checkbox>`,
})
export const Checked = TemplateChecked.bind({})
Checked.args = {
  label: 'Label do checkbox',
  checked: true,
  vModel: 'check3',
}
Checked.parameters = {
  controls: {
    exclude: defaultExcluded,
  },
}

export const Invalid = Template.bind({})
Invalid.args = {
  label: 'Label do checkbox',
  format: 'invalid',
}
Invalid.parameters = {
  controls: {
    exclude: defaultExcluded,
  },
}

export const Valid = Template.bind({})
Valid.args = {
  label: 'Label do checkbox',
  format: 'valid',
}
Valid.parameters = {
  controls: {
    exclude: defaultExcluded,
  },
}

export const Disabled = Template.bind({})
Disabled.args = {
  label: 'Label do checkbox',
  disabled: true,
}
Disabled.parameters = {
  controls: {
    exclude: defaultExcluded,
  },
}

const TemplateMultiple = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<div>
  <p class="label mb-0">Rótulo</p>
  <p class="text-down-01">Informações adicionais</p>
  <div class="mb-1">
    <br-checkbox label="Unchecked"></br-checkbox>
  </div>
  <div class="mb-1">
    <br-checkbox label="Checked" checked></br-checkbox>
  </div>
  <div class="mb-1">
    <br-checkbox label="Valid" format="valid"></br-checkbox>
  </div>
  <div class="mb-1">
    <br-checkbox label="Invalid" format="invalid"></br-checkbox>
  </div>
  <div class="mb-1">
    <br-checkbox label="Disabled" disabled></br-checkbox>
  </div>
</div>`,
})

export const Multiple = TemplateMultiple.bind({})
Multiple.parameters = {
  controls: {
    exclude: [
      ...defaultExcluded,
      'label',
      'disabled',
      'inline',
      'value',
      'v-model',
      'checked',
    ],
  },
}

const TemplateInline = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<div>
  <p class="label mb-0">Rótulo</p>
  <p class="text-down-01">Informações adicionais</p>
  <br-checkbox label="Unchecked" inline></br-checkbox>
  <br-checkbox label="Checked" checked inline></br-checkbox>
  <br-checkbox label="Valid" format="valid" inline></br-checkbox>
  <br-checkbox label="Invalid" format="invalid" inline></br-checkbox>
  <br-checkbox label="Disabled" disabled inline></br-checkbox>
</div>
  `,
})

export const Inline = TemplateInline.bind({})
Inline.parameters = {
  controls: {
    exclude: [
      ...defaultExcluded,
      'label',
      'disabled',
      'inline',
      'value',
      'v-model',
      'checked',
    ],
  },
}

const TemplateCheckgroup = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-checkgroup ${args.label ? `label="${args.label}"` : ''}>${
    args.default
  }</br-checkgroup>
  `,
})

export const Checkgroup = TemplateCheckgroup.bind({})
Checkgroup.parameters = {
  controls: {
    exclude: [
      'disabled',
      'format',
      'inline',
      'ariaLabel',
      'name',
      'checked',
      'value',
      'indeterminate',
      'checkgroupHeader',
      'change',
    ],
  },
}

Checkgroup.args = {
  default: `<br-checkbox label="Opção 1"></br-checkbox>
<br-checkbox label="Opção 2"></br-checkbox>
<br-checkbox label="Opção 3"></br-checkbox>`,
}

Checkgroup.argTypes = {
  label: {
    description: 'Texto descritivo do grupo de caixas de opções.',
    table: {
      type: {
        summary: `string`,
      },
      defaultValue: {
        summary: 'Selecionar tudo',
      },
    },
  },
  default: {
    description:
      '**[OBRIGATÓRIO]** Conteúdo do br-checkgroup, que devem ser nós do tipo **<br-checkbox>** passados por slot.',
    control: 'text',
    type: {
      required: true,
    },
    table: {
      type: {
        summary: 'NodeList of <br-checkbox>',
      },
      defaultValue: {
        summary: '',
      },
    },
  },
}
