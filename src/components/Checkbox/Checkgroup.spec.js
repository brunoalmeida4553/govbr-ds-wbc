import { shallowMount } from '@vue/test-utils'
import Checkbox from './Checkbox.ce.vue'
import Checkgroup from './Checkgroup.ce.vue'

describe('Checkgroup', () => {
  test('it renders a br-checkbox[checkgroup-header]', () => {
    const wrapper = shallowMount(Checkgroup, {
      slots: {
        default: shallowMount(Checkbox),
      },
    })

    expect(wrapper.find('br-checkbox[checkgroup-header]').exists()).toBe(true)
  })
})
