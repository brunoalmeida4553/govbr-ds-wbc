import { customEventStorybookArgType, kebabiseArgs } from '../../util/Utils.js'
import Tab from './Tab.ce.vue'
import BrTabItem from './TabItem.ce.vue'

const slotDefault = `
  <br-tab-item title="Sobre" id="panel-1"><p>Sobre</p></br-tab-item>
  <br-tab-item title="Todos" id="panel-2"><p>Todos</p></br-tab-item>
  <br-tab-item title="Notícias" id="panel-3"><p>Notícias</p></br-tab-item>
  <br-tab-item title="Serviços" id="panel-4"><p>Serviços</p></br-tab-item>
`

const slotComIcone = `
  <br-tab-item title="Sobre" id="panel-1" icon="home"><p>Sobre</p></br-tab-item>
  <br-tab-item title="Todos" id="panel-2" icon="image"><p>Todos</p></br-tab-item>
  <br-tab-item title="Notícias" id="panel-3" icon="image"><p>Notícias</p></br-tab-item>
  <br-tab-item title="Serviços" id="panel-4" icon="image"><p>Serviços</p></br-tab-item>
`

const slotComContador = `
  <br-tab-item title="Sobre" id="panel-1" results="120">
    <p>Sobre</p>
  </br-tab-item>
  <br-tab-item title="Todos" id="panel-2" results="124">
    <p>Todos</p>
  </br-tab-item>
  <br-tab-item title="Notícias" id="panel-3" results="74">
    <p>Notícias</p>
  </br-tab-item>
  <br-tab-item title="Serviços" id="panel-4" results="16">
    <p>Serviços</p>
  </br-tab-item>
`

export default {
  title: 'Componentes/Tab',
  component: Tab,
  subcomponents: {
    'br-tab-item': BrTabItem,
  },
  parameters: {
    backgrounds: {
      default: 'light',
      values: [
        { name: 'light', value: '#fff' },
        { name: 'dark', value: '#071D41' },
      ],
    },
  },
  argTypes: {
    density: {
      control: {
        type: 'select',
        options: ['small', 'medium', 'large'],
      },
    },
    default: {
      description:
        '**[OBRIGATÓRIO]** Conteúdo da notificação, que devem ser nós do tipo **<br-tab-item>** passados por slot.',
      control: 'text',
      type: {
        required: true,
      },
      table: {
        type: {
          summary: 'NodeList of <br-tab-item>',
        },
        defaultValue: {
          summary: '',
        },
      },
    },
    inverted: {
      defaultValue: false,
    },
    label: {
      defaultValue: '',
    },
    onTabItemClick: {
      ...customEventStorybookArgType,
    },
  },
}

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-tab v-bind="args">${args.default}</br-tab>`,
})

export const Default = Template.bind({})
Default.args = {
  default: slotDefault,
}

export const Icon = Template.bind({})
Icon.args = {
  default: slotComIcone,
}

export const Contador = Template.bind({})
Contador.args = {
  default: slotComContador,
  counter: true,
}
