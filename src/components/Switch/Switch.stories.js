import { kebabiseArgs } from '../../util/Utils.js'
import BrSwitch from './Switch.ce.vue'

const slotDefault = `
  Este label foi passado por slot
`
export default {
  title: 'Componentes/Switch',
  component: BrSwitch,
  argTypes: {
    checked: {
      control: 'boolean',
      description: 'Determina se o switch está checado.',
      defaultValue: false,
    },
    disabled: {
      description: 'Determina se o switch está desabilitado.',
      defaultValue: false,
    },
    icon: {
      control: 'boolean',
      description:
        'Determina se o switch possui ícones representativos para o checked.',
      defaultValue: false,
    },
    size: {
      control: { type: 'select', options: ['small', 'medium', 'large'] },
      description: 'Opções de tamanho do componente (small, medium e large).',
    },
    label: {
      control: 'text',
      description: 'Rótulo do componente.',
      defaultValue: '',
    },
    default: {
      control: 'text',
      description:
        'Rótulo do componente passado como slot (prevalece sobre o atributo "label").',
      defaultValue: '',
      table: {
        type: {
          summary: 'NodeList',
        },
        defaultValue: {
          summary: '',
        },
      },
    },
    labelChecked: {
      control: 'text',
      description: 'Rótulo para o checked true.',
      defaultValue: '',
    },
    labelNotChecked: {
      control: 'text',
      description: 'Rótulo para o checked false.',
      defaultValue: '',
    },
    top: {
      defaultValue: false,
    },
    right: {
      defaultValue: false,
    },
  },
}

const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-switch v-bind="args"></br-switch>`,
})

const TemplateComSlot = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-switch v-bind="args">${args.default}</br-switch>`,
})

export const Completo = TemplateComSlot.bind({})
Completo.args = {
  icon: true,
  checked: true,
  size: 'large',
  labelChecked: 'Sucesso',
  labelNotChecked: 'Erro',
  default: slotDefault,
}

export const Simples = Template.bind({})
Simples.args = {
  label: 'Simples',
}
