import { shallowMount } from '@vue/test-utils'
import BrButton from '../Button/Button.ce.vue'
import IconBase from '../IconBase/IconBase.ce.vue'
import SignIn from './SignIn.ce.vue'

const densidades = ['large', 'middle', 'small']
const tipo = ['secondary', 'primary']

describe('SignIn', () => {
  const wrapperInternalSignIn = shallowMount(SignIn, {
    propsData: {
      icon: 'user',
      label: 'Entrar',
    },
    global: {
      stubs: {
        'icon-base': IconBase,
        'br-button': BrButton,
      },
    },
  })
  test('it renders br-button', () => {
    expect(wrapperInternalSignIn.classes('br-button')).toBe(true)
  })
  test('it renders icon', () => {
    expect(wrapperInternalSignIn.find('.fas.fa-user').exists()).toBe(true)
  })
  test('it renders label', () => {
    expect(wrapperInternalSignIn.text()).toMatch('Entrar')
  })

  const wrapperExternalSignInWithImage = shallowMount(SignIn, {
    propsData: {
      icon: 'user',
      label: 'Entrar com',
      image:
        "{ 'url': 'https://www.gov.br/++theme++padrao_govbr/img/govbr-colorido-b.png', 'description': 'gov.br' }",
    },
    global: {
      stubs: {
        'icon-base': IconBase,
        'br-button': BrButton,
      },
    },
  })
  test('it renders image', () => {
    expect(
      wrapperExternalSignInWithImage
        .find(
          'img[src="https://www.gov.br/++theme++padrao_govbr/img/govbr-colorido-b.png"]'
        )
        .exists()
    ).toBe(true)
  })
  test('it not renders icon', () => {
    expect(wrapperExternalSignInWithImage.find('.fa-user').exists()).toBe(false)
  })

  const wrapperExternalSignInWithName = shallowMount(SignIn, {
    propsData: {
      icon: 'user',
      label: 'Entrar com',
      image:
        "{ 'url': 'https://www.gov.br/++theme++padrao_govbr/img/govbr-colorido-b.png', 'description': 'gov.br' }",
      entity: 'gov.br',
    },
    global: {
      stubs: {
        'icon-base': IconBase,
        'br-button': BrButton,
      },
    },
  })
  test('it not renders image', () => {
    expect(
      wrapperExternalSignInWithName
        .find(
          'img[src="https://www.gov.br/++theme++padrao_govbr/img/govbr-colorido-b.png"]'
        )
        .exists()
    ).toBe(false)
  })
  test('it renders entity name', () => {
    expect(wrapperExternalSignInWithName.text()).toMatch('gov.br')
  })
  test('it not renders icon', () => {
    expect(wrapperExternalSignInWithName.find('.fa-user').exists()).toBe(false)
  })

  const wrapperIconicSignIn = shallowMount(SignIn, {
    propsData: {
      iconic: true,
      icon: 'user',
      label: 'Entrar com',
    },
    global: {
      stubs: {
        'icon-base': IconBase,
        'br-button': BrButton,
      },
    },
  })
  test('it has class circle', () => {
    expect(wrapperIconicSignIn.classes('circle')).toBe(true)
  })
  test('it renders icon', () => {
    expect(wrapperIconicSignIn.find('.fa-user').exists()).toBe(true)
  })
  test('it not renders label', () => {
    expect(wrapperIconicSignIn.text()).not.toMatch('Entrar com')
  })

  densidades.forEach((densidade) => {
    test(`set density attribute ${densidade}`, () => {
      const wrapperSignInDensity = shallowMount(SignIn, {
        propsData: {
          icon: 'user',
          label: 'Entrar',
          density: densidade,
        },
        global: {
          stubs: {
            'icon-base': IconBase,
            'br-button': BrButton,
          },
        },
      })
      expect(wrapperSignInDensity.find(`.${densidade}`).exists()).toBe(true)
    })
  })

  tipo.forEach((cor) => {
    test(`set type attribute ${cor}`, () => {
      const wrapperSignInType = shallowMount(SignIn, {
        propsData: {
          type: cor,
        },
        global: {
          stubs: {
            'icon-base': IconBase,
            'br-button': BrButton,
          },
        },
      })
      expect(wrapperSignInType.find(`.${cor}`).exists()).toBe(true)
    })
  })

  const wrapperSignInInverted = shallowMount(SignIn, {
    propsData: {
      inverted: true,
      icon: 'user',
      label: 'Entrar',
    },
    global: {
      stubs: {
        'icon-base': IconBase,
        'br-button': BrButton,
      },
    },
  })
  test('it renders inverted', () => {
    expect(wrapperSignInInverted.classes('inverted')).toBe(true)
  })

  const wrapperSignInBlock = shallowMount(SignIn, {
    propsData: {
      block: true,
      icon: 'user',
      label: 'Entrar',
    },
    global: {
      stubs: {
        'icon-base': IconBase,
        'br-button': BrButton,
      },
    },
  })
  test('it renders inverted', () => {
    expect(wrapperSignInBlock.classes('block')).toBe(true)
  })
})
