import { kebabiseArgs } from '../../util/Utils.js'
import Signin from './SignIn.ce.vue'

export default {
  title: 'Componentes/Signin',
  component: Signin,
  parameters: {
    backgrounds: {
      default: 'light',
      values: [
        { name: 'light', value: '#fff' },
        { name: 'dark', value: '#071D41' },
      ],
    },
  },
  argTypes: {
    label: {
      control: 'text',
      defaultValue: null,
    },
    type: {
      control: {
        type: 'select',
        options: ['secondary', 'primary'],
      },
      defaultValue: 'secondary',
    },
    density: {
      control: {
        type: 'select',
        options: ['large', 'middle', 'small'],
      },
      defaultValue: 'middle',
    },
    iconic: {
      defaultValue: false,
    },
    block: {
      defaultValue: false,
    },
    icon: {
      defaultValue: null,
    },
    inverted: {
      defaultValue: false,
    },
    image: {
      defaultValue: null,
    },
    entity: {
      defaultValue: null,
    },
  },
}
const Template = (args) => ({
  props: Object.keys(args),
  setup() {
    return { args: kebabiseArgs(args) }
  },
  template: `<br-sign-in v-bind="args"></br-sign-in>`,
})

export const Interno = Template.bind({})
Interno.args = {
  type: 'primary',
  density: 'middle',
  icon: 'fa-user',
  label: 'Entrar',
}

export const Externo = Template.bind({})
Externo.args = {
  type: 'primary',
  density: 'middle',
  label: 'Entrar com',
  image:
    "{ 'url': 'https://www.gov.br/++theme++padrao_govbr/img/govbr-colorido-b.png', 'description': 'gov.br' }",
  entity: 'gov.br',
}

export const Iconico = Template.bind({})
Iconico.args = {
  iconic: true,
  type: 'primary',
  density: 'middle',
  icon: 'fa-user',
}
