<!--
- O MR para ser analisado precisa seguir algumas regras básicas, como correta nomenclatura, categorização e conduta do criador.
- Por favor observe os requisitos na nossa Wiki <https://govbr-ds.gitlab.io/govbr-ds-wiki/> "Merge Request - Criação" antes de começar a criar MR.
- Se tiver dúvidas, por favor pergunte! Podemos te ajudar.
-->

## Descrição

<!-- Explique a motivação desse Merge Request. -->

## Issues relacionadas

<!--
- Esse projeto somente aceita MRs relativos a issues abertas.
- Caso seja uma sugestão ou melhoria, por favor discuta em uma issue primeiro.
- Caso seja uma correção, uma issue deve ser criada descrevendo o problema e como reproduzir.
-->

## Atualizados

[ ] Testes JEST

[ ] Pasta _public_

[ ] Stories do storybook

[ ] Documentação do componente

## Testes

<!--
- Descreva em detalhes os testes realizados.
- Inclua quais os testes foram realizados, como foram executados e como as alterações afetaram outras partes do projeto.
 -->

### Testou nos seguintes projetos

[ ]   Quickstart Angular

[ ]   Quickstart React

[ ]   Quickstart Vanila

[ ]   Quickstart Vue

<!-- Includa outroas onde tiver feito o teste -->

## Screenshots (opcional)

<!-- Inclua screenshots relacionados a esse código caso considere necessário -->

## Classifique seu código

<!-- Que tipos de alterações esse código introduz? Inclua as tags/etiquetas se que aplicam ao contexto desse MR -->
